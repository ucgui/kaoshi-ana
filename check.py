# -*- coding: utf-8 -*-  

import urllib2
import time
import pygame  # pip install pygame
import datetime

def checkTongZhao():
    # User-Agent是爬虫与反爬虫的第一步
    ua_headers = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36'}
    # 通过urllib2.Request()方法构造一个请求对象
    url = 'http://www.zhjy.gov.cn/zwgk/gsgg/'
    #request = urllib2.Request('http://www.baidu.com/',headers=ua_headers)
    request = urllib2.Request(url, headers=ua_headers)

    #向指定的url地址发送请求，并返回服务器响应的类文件对象
    response = urllib2.urlopen(request)
    # 服务器返回的类文件对象支持python文件对象的操作方法
    # read()方法就是读取文件里的全部内容，返回字符串
    html = response.read()

    if (html.find("统招") >= 0):
        print html
        return True
    return False



# 貌似只能播放单声道音乐，可能是pygame模块限制
def playMusic(filename, loops=0, start=0.0, value=0.5):
    """
    :param filename: 文件名
    :param loops: 循环次数
    :param start: 从多少秒开始播放
    :param value: 设置播放的音量，音量value的范围为0.0到1.0
    :return:
    """
    flag = False  # 是否播放过
    pygame.mixer.init()  # 音乐模块初始化
    while 1:
        if flag == 0:
            pygame.mixer.music.load(filename)
            # pygame.mixer.music.play(loops=0, start=0.0) loops和start分别代表重复的次数和开始播放的位置。
            pygame.mixer.music.play(loops=loops, start=start)
            pygame.mixer.music.set_volume(value)  # 来设置播放的音量，音量value的范围为0.0到1.0。
        if pygame.mixer.music.get_busy() == True:
            flag = True
        else:
            if flag:
                pygame.mixer.music.stop()  # 停止播放
                break


while True:    
    print(time.strftime('%Y-%m-%d %X',time.localtime()))    
    if (checkTongZhao() == True): # 此处为要执行的任务
        playMusic("bigeasy.ogg")
    time.sleep(60*10)

